package com.alex.calcdemo;

import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class CalcDemoApplicationTests {

    @Test
    void testPrecision() {
        System.out.println(Precision.round(0.146, 2));
    }

    private int getFullYearNumber(int monthNumber) {
        if (monthNumber <= 12) return 1;

        int v = monthNumber / 12;
        return monthNumber % 12 == 0 ? v : v + 1;
    }

    private int getFullMonthNumber(int monthNumber) {
        if (monthNumber <= 12) return monthNumber;
        return monthNumber - (getFullYearNumber(monthNumber) - 1) * 12;
    }

    private String getMonthYearStringByMonthAmount(int monthNumber) {
        return String.format("%d год %d месяц", getFullYearNumber(monthNumber),
                getFullMonthNumber(monthNumber));
    }

    @Test
    void monthFormat() {
        int month = 13;

        System.out.println(getMonthYearStringByMonthAmount(month));
    }
}
