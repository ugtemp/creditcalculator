$(document).ready(function () {
    $('#sum').val('100000');
    $('#duration').val('12');

    $('.number').keypress(function(event){
        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault(); //stop character from entering input
        }
    });

    $('#calcButton').click(function (event) {
        var sum = parseInt($('#sum').val());
        var duration = parseInt($('#duration').val());

        if (isNaN(sum)) {
            alert("Заполните сумму кредита");
            event.preventDefault();
            return;
        }

        if (isNaN(duration)) {
            alert("Заполните срок кредита");
            event.preventDefault();
            return;
        }

        if (sum < 1e5 || sum > 5e6) {
            alert("Сумма кредита должна быть в диапазоне от 100 000 руб до 5 000 000 руб");
            $('#sum').val('100000');
            event.preventDefault();
            return;
        }

        if (duration < 12 || duration > 60) {
            alert("Срок кредита должен быть в диапазоне от 12 до 60 мес");
            $('#duration').val('12');
            event.preventDefault();
        }
    });
});