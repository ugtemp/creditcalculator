package com.alex.calcdemo.controller;

import com.alex.calcdemo.dto.CreditCondition;
import com.alex.calcdemo.service.CalcService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CalcController {
    private static final Log LOG = LogFactory.getLog(CalcController.class);
    private final CalcService calcService;

    public CalcController(CalcService calcService) {
        this.calcService = calcService;
    }

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("creditCondition", new CreditCondition());
        return "main";
    }

    @PostMapping("/calc")
    public String calculate(CreditCondition condition, BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOG.error(result.getAllErrors());
        }
        model.addAttribute("calcResult", calcService.calculate(condition));
        return "result";
    }
}
