package com.alex.calcdemo.dto;

public class CreditCondition {
    /**
     * Сумма кредита
     */
    private int sum;

    /**
     * Срок кредита в месяцах
     */
    private int duration;

    public CreditCondition() {
    }

    public CreditCondition(int sum, int duration) {
        this.sum = sum;
        this.duration = duration;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
