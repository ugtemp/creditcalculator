package com.alex.calcdemo.dto;

public class PaymentFeatures {

    /**
     * Номер платежа
     */
    private int id;
    /**
     * Месяц/год
     */
    private String monthNumber;

    /**
     * Платеж по основному долгу
     */
    private double principalPayment;

    /**
     * Платеж по процентам
     */
    private double interestPayment;

    /**
     * Остаток основного долг
     */
    private double balance;

    /**
     * Общая сумма платежа
     */
    private double payed;

    public PaymentFeatures() {
    }

    public PaymentFeatures(int id, double principalPayment, double interestPayment,
                           double balance) {
        this.id = id;
        this.principalPayment = principalPayment;
        this.interestPayment = interestPayment;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMonthNumber() {
        return getMonthYearStringByMonthAmount(id);
    }

    public double getPrincipalPayment() {
        return principalPayment;
    }

    public void setPrincipalPayment(double principalPayment) {
        this.principalPayment = principalPayment;
    }

    public double getInterestPayment() {
        return interestPayment;
    }

    public void setInterestPayment(double interestPayment) {
        this.interestPayment = interestPayment;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getPayed() {
        return payed;
    }

    public void setPayed(double payed) {
        this.payed = payed;
    }

    @Override
    public String toString() {
        return "PaymentFeatures{" +
                "id=" + id +
                ", monthNumber=" + monthNumber +
                ", principalPayment=" + principalPayment +
                ", interestPayment=" + interestPayment +
                ", balance=" + balance +
                '}';
    }

    private int getFullYearNumber(int monthNumber) {
        if (monthNumber <= 12) return 1;

        int v = monthNumber / 12;
        return monthNumber % 12 == 0 ? v : v + 1;
    }

    private int getFullMonthNumber(int monthNumber) {
        if (monthNumber <= 12) return monthNumber;
        return monthNumber - (getFullYearNumber(monthNumber) - 1) * 12;
    }

    private String getMonthYearStringByMonthAmount(int monthNumber) {
        return String.format("%d год %d месяц", getFullYearNumber(monthNumber),
                getFullMonthNumber(monthNumber));
    }
}
