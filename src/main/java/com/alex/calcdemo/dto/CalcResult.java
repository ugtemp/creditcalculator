package com.alex.calcdemo.dto;

import java.util.ArrayList;
import java.util.List;

public class CalcResult {
    /**
     * Ежемесячный платеж
     */
    private double monthlyPayment;
    /**
     * Ежемесячные показатели платежа
     */
    private final List<PaymentFeatures> paymentFeatures = new ArrayList<>();

    public CalcResult() {
    }

    public void setMonthlyPayment(double paymnet) {
        this.monthlyPayment = paymnet;
    }

    public double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void addPaymentFeature(PaymentFeatures features) {
        paymentFeatures.add(features);
    }

    public List<PaymentFeatures> getPaymentFeatures() {
        return paymentFeatures;
    }
}
