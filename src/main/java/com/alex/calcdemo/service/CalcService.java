package com.alex.calcdemo.service;

import com.alex.calcdemo.dto.CalcResult;
import com.alex.calcdemo.dto.CreditCondition;

public interface CalcService {
    /**
     * Процентная ставка кредита
     */
    double RATE = 12.9;

    CalcResult calculate(CreditCondition condition);
}
