package com.alex.calcdemo.service;

import com.alex.calcdemo.dto.CalcResult;
import com.alex.calcdemo.dto.CreditCondition;
import com.alex.calcdemo.dto.PaymentFeatures;
import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Service;

@Service
public class DefaultCalcService implements CalcService {
    @Override
    public CalcResult calculate(CreditCondition condition) {
        double p = RATE / 100 / 12;
        int s = condition.getSum();
        int n = condition.getDuration();
        double monthlyPayment = s * (p + p / (Math.pow(1 + p, n) - 1));

        CalcResult result = new CalcResult();
        result.setMonthlyPayment(Precision.round(monthlyPayment, 2));
        double balance = s;

        result.addPaymentFeature(new PaymentFeatures(1, 0, 0, Precision.round(balance, 2)));

        for (int i = 1; i <= n; i++) {
            double interestPayment = balance * p;
            double principalPayment = monthlyPayment - interestPayment;
            balance -= principalPayment;

            PaymentFeatures paymentFeatures = new PaymentFeatures(i + 1,
                    Precision.round(principalPayment, 2),
                    Precision.round(interestPayment, 2), Precision.round(balance, 2));
            paymentFeatures.setPayed(Precision.round(condition.getSum() - balance, 2));
            result.addPaymentFeature(paymentFeatures);
        }
        return result;
    }
}
